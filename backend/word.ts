import { AlignmentType, Document, HeadingLevel, Packer, Paragraph, TabStopPosition, TabStopType, TextRun } from "docx";
import * as fs from "fs"
import resume from "../resume.json"

const PHONE_NUMBER = "07534563401";
const PROFILE_URL = "https://www.linkedin.com/in/dolan1";
const EMAIL = "docx@com";


class DocumentCreator {
    // tslint:disable-next-line: typedef
    public create(): Document {
        const {work, education,awards, skills, interests, languages, basics} = resume;
        const locationString =`${basics.location.address} ${basics.location.postalCode} ${basics.location.city}`
        const document = new Document({
            sections: [
                {
                    children: [
                        new Paragraph({
                            text: basics.name,
                            heading: HeadingLevel.TITLE,
                        }),
                        this.createContactInfo(basics.phone, basics.url, basics.email, locationString),
                        this.createHeading("Experience"),
                        ...work
                            .map((position) => {
                                const arr: Paragraph[] = [];

                                arr.push(
                                    this.createInstitutionHeader(
                                        position.name,
                                        this.createPositionDateText(position.startDate, position.endDate),
                                    ),
                                );
                                arr.push(this.createRoleText(position.position));

                                position.highlights.forEach((bulletPoint) => {
                                    arr.push(this.createBullet(bulletPoint));
                                });
                                arr.push(this.createBullet(position.technologies.join(", ")))

                                return arr;
                            })
                            .reduce((prev, curr) => prev.concat(curr), []),
                            this.createHeading("Education"),
                            ...education
                                .map((step) => {
                                    const arr: Paragraph[] = [];
                                    arr.push(
                                        this.createInstitutionHeader(
                                            step.institution,
                                            `${step.startDate} - ${step.endDate}`,
                                        ),
                                    );
                                    arr.push(this.createRoleText(`${step.area} - ${step.studyType}`));
    
                                    step.courses.forEach((bulletPoint) => {
                                        arr.push(this.createBullet(bulletPoint));
                                    });
    
                                    return arr;
                                })
                                .reduce((prev, curr) => prev.concat(curr), []),
                        this.createHeading("Awards, Skills and Interests"),
                        this.createSubHeading("Skills"),
                        ...this.createAwardList(awards),
                        this.createSubHeading("Skills"),
                        ...this.createSkillsList(skills),
                        this.createSubHeading("Interests"),
                        ...this.createSkillsList(interests),
                        new Paragraph({
                            text:
                                "This CV was generated from juliandemourgues.com",
                            alignment: AlignmentType.CENTER,
                        }),
                        new Paragraph({
                            text:
                                "Check it out for my portfolio and more!",
                            alignment: AlignmentType.CENTER,
                        }),
                    ],
                },
            ],
        });

        return document;
    }

    public createContactInfo(phoneNumber: string, profileUrl: string, email: string, locationString: string): Paragraph {
        return new Paragraph({
            alignment: AlignmentType.CENTER,
            children: [
                new TextRun(`Mobile: ${phoneNumber} | LinkedIn: ${profileUrl} | Email: ${email}`),
                new TextRun({
                    text: locationString,
                    break: 1,
                }),
            ],
        });
    }

    public createHeading(text: string): Paragraph {
        return new Paragraph({
            text: text,
            heading: HeadingLevel.HEADING_1,
            thematicBreak: true,
        });
    }

    public createSubHeading(text: string): Paragraph {
        return new Paragraph({
            text: text,
            heading: HeadingLevel.HEADING_2,
        });
    }

    public createInstitutionHeader(institutionName: string, dateText: string): Paragraph {
        return new Paragraph({
            tabStops: [
                {
                    type: TabStopType.RIGHT,
                    position: TabStopPosition.MAX,
                },
            ],
            children: [
                new TextRun({
                    text: institutionName,
                    bold: true,
                }),
                new TextRun({
                    text: `\t${dateText}`,
                    bold: true,
                }),
            ],
        });
    }

    public createRoleText(roleText: string): Paragraph {
        return new Paragraph({
            children: [
                new TextRun({
                    text: roleText,
                    italics: true,
                }),
            ],
        });
    }

    public createBullet(text: string): Paragraph {
        return new Paragraph({
            text: text,
            bullet: {
                level: 0,
            },
        });
    }

    // tslint:disable-next-line:no-any
    public createAwardList(awards: any[]): Paragraph[] {
        return awards.map(
            (award) =>
                new Paragraph({
                    children: [
                        new TextRun({
                            text: award.title,
                            bold: true,
                        }),
                        new TextRun({
                            text: ` (${award.awarder} ${this.getDateText(award.date)})`,
                        }),
                    ],
                    bullet: {
                        level: 0,
                    },
                }),
        );
    }

    // tslint:disable-next-line:no-any
    public createSkillsList(achivements: any[]): Paragraph[] {
        return achivements.map(
            (achievement) =>
            new Paragraph({
                children: [
                    new TextRun({
                        text: achievement.name,
                        bold: true,
                    }),
                    new TextRun({
                        text: ` (${achievement.keywords.join(", ")})`
                    }),
                ],
                bullet: {
                    level: 0,
                },
            })
        );
    }

    public createInterests(interests: string): Paragraph {
        return new Paragraph({
            children: [new TextRun(interests)],
        });
    }

    public getDateText(date: string) {
        if (date) {
            const dateObj = new Date(date)
            return this.getMonthFromInt(dateObj.getMonth()) + " " + dateObj.getFullYear()    
        }
        return "Today"
    }   

    // tslint:disable-next-line:no-any
    public createPositionDateText(startDate: any, endDate: any): string {
        const endDateText = this.getDateText(endDate)
        const startDateText = this.getDateText(startDate)
        return `${startDateText} - ${endDateText}`;
    }
    public getMonthFromInt(value: number): string {
        switch (value) {
            case 1:
                return "Jan";
            case 2:
                return "Feb";
            case 3:
                return "Mar";
            case 4:
                return "Apr";
            case 5:
                return "May";
            case 6:
                return "Jun";
            case 7:
                return "Jul";
            case 8:
                return "Aug";
            case 9:
                return "Sept";
            case 10:
                return "Oct";
            case 11:
                return "Nov";
            case 12:
                return "Dec";
            default:
                return "Jan";
        }
    }
}
const documentCreator = new DocumentCreator();

const doc = documentCreator.create();

Packer.toBuffer(doc).then((buffer) => {
    fs.writeFileSync("CV.docx", buffer);
});