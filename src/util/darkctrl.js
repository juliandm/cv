import { useCallback, useState } from "react"

export default () => {
  const [dark, setDark] = useState(true)
  const toggleDark = useCallback(() => setDark((d) => !d), [])
  return { dark, toggleDark }
}
